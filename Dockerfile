FROM python:3.10
COPY req.txt /app/
WORKDIR /app
RUN pip install --no-cache-dir -r req.txt
COPY . /app/
WORKDIR /app/frontend
EXPOSE 3000
CMD ["streamlit", "run", "./app.py", "--server.port", "3000"]
