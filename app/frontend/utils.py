import os
import pandas as pd
import streamlit as st
import easyocr
import cv2
import numpy as np


def perform_ocr(image):
    reader = easyocr.Reader(['ru', 'en'], gpu=False)
    result = reader.readtext(image, blocklist=';:.,()', width_ths=0.7, decoder='beamsearch')
    return result


def draw_rectangles_on_image(image, result):
    for (coord, _, _) in result:
        (topleft, bottomright) = (coord[0], coord[3])
        tx, ty = (int(topleft[0]), int(topleft[1]))
        bx, by = (int(bottomright[0]), int(bottomright[1]))
        cv2.rectangle(image, (tx, ty), (bx, by), (255, 0, 0), 2)
        return image

def display_images_and_results(image, processed_image, result):
    st.image(image, caption="Загруженное изображение.", use_column_width=True)
    st.image(processed_image, caption="Обработанное изображение.", use_column_width=True)

    list_txt = [(coord, text, prob) for (coord, text, prob) in result]
    df = pd.DataFrame(list_txt, columns=['Координаты', 'Текст', 'Уверенность модели'])
    st.write(df)


def save_results_to_file(result, uploaded_file):
    list_txt = []
    for (coord, text, prob) in result:
        list_txt.append([coord, text, prob])

    df = pd.DataFrame(list_txt, columns=['Координаты', 'Текст', 'Уверенность модели'])

    if st.button("Сохранить в файл"):
        base_filename = os.path.splitext(uploaded_file.name)[0]
        output_filename = f"{base_filename}.txt"
        df.to_csv(output_filename, index=False, sep='\t', encoding='utf-8')
        st.success(f"Результаты OCR сохранены в файл: {output_filename}")

