import os
import cv2
import numpy as np
import pandas as pd
import easyocr
import argparse


def process_images(input_folder):
    reader = easyocr.Reader(['ru', 'en'], gpu=False)

    for filename in os.listdir(input_folder):
        if filename.endswith(('.jpg', '.jpeg', '.png', '.jp2')):
            image_path = os.path.join(input_folder, filename)
            image = cv2.imread(image_path)

            result = reader.readtext(image, blocklist=';:.,()', width_ths=0.7, decoder='beamsearch')

            for (coord, text, prob) in result:
                (topleft, topright, bottomright, bottomleft) = coord
                tx, ty = (int(topleft[0]), int(topleft[1]))
                bx, by = (int(bottomright[0]), int(bottomright[1]))
                cv2.rectangle(image, (tx, ty), (bx, by), (255, 0, 0), 2)

            list_txt = []
            for (coord, text, prob) in result:
                list_txt.append(text)

            output_filename = os.path.join(input_folder, os.path.splitext(filename)[0] + ".txt")
            with open(output_filename, 'w') as file:
                for item in list_txt:
                    file.write("%s\n" % item)
            print(f"saved {output_filename}")


if __name__ == "__main__":
    parser = argparse.ArgumentParser(description="Process images and save results to text files.")
    parser.add_argument("input_folder", help="Path to the folder containing images.")
    args = parser.parse_args()

    process_images(args.input_folder)
