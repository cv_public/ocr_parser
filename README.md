# Волостной сигнализатор (python 3.10)

Задача сбора фамилий жителей волостей (https://archive.org/details/CertificatesofConvictionImperialRussia) - 200 ГБ данных за 3 века

Проводилась программная работа с текстовыми данными (txt) и с визуальными данными (сканы старых книг)

Подзадачи:

Сбор фамилий осужденных жителей волостей

Сбор фамилий воевавших жителей волостей

В папке EDA представлен анализ задачи и презентация

![img.png](pics/img.png)

## Клонирование проекта

```shell
git clone git@gitlab.com:cv_public/ocr_parser.git
```

## Установка зависимостей (pipenv)

Если в системе нету python3.10, то pipenv не сможет сам его установить - https://www.linuxcapable.com/how-to-install-python-3-10-on-ubuntu-linux/

**Внимание** - библиотеки занимают 2гб памяти

```shell
cd ocr_parser

pip install pipenv

pipenv shell

pipenv install
```

## Запуск визуализации

```shell
# если вышли из cmd - нужно заново будет зайти в виртуальное окружение pipenv shell
streamlit run ./app/frontend/app.py
```

## Запуск обработки по папкам



```shell
# если вышли из cmd - нужно заново будет зайти в виртуальное окружение pipenv shell
python ./app/frontend/app_cli.py /home/storonkin/Загрузки/OCR/01005382072_jp2
```

## Как сделать CI CD гитлаб

README в папке docker_runner

