<h1>Установка и настройка Gitlab</h1>
На сервере, где будет целевой деплой (можно локально) создать файл dockercompose.yml для локального деплоя раннера. <br><br>
В терминале выполнить docker-compose up -d , подождать пока деплой завершится
(проверить командой docker ps ) <br><br>
В Gitlab в желаемом проекте перейти в раздел Settings - CI/CD, там раскрыть пункт
Runners. Скопировать registration token <br><br>

В терминале выполнить команду для инициализации раннера: <br>
docker exec -it gitlab-runner gitlab-runner register --url "урл_гитлаба" --clone-url
"урл_гитлаба" <br>
URL вставить нужный - https://gitlab.com/ для публичного Гитлаба или другой, 
если используется другой экземпляр; <br>
Description= имя раннера, можно любое (для удобной отладки)
Вставить токен, скопированный ранее; <br>
тэг придумать посложнее (например docker_0910), чтоб не совпало с
дефолтными раннерами Гитлаба; <br>
maintenance note оставить пустым; <br>
тип раннера выбрать docker ; <br>
дефолтный образ - docker:stable <br><br>

После первичной иниализации раннера, необходимо его донастроить: залогиниться
внутрь контейнера через docker exec -it gitlab-runner bash и открыть файл /etc/gitlab-
runner/config.toml <br><br> В конце файла дописать строчку: <b>volumes = ["/var/run/docker.sock:/var/run/docker.sock", "/cache"]</b> <br><br>
Выполнить <b>docker restart gitlab-runner</b> <br><br>

Вернуться в веб-интерфейс Gitlab и убедиться, что раннер “подцепился” (внизу в
разделе Assigned project runners)
